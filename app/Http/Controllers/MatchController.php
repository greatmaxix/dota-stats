<?php

namespace App\Http\Controllers;

use App\Models\Match;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;

class MatchController extends Controller
{
    public function shower($match){
        return view('match.show')->with('match',Match::shower($match));
    }
}
