<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function show(User $user){
        return view('profile.show')->with(compact('user'));
    }

    public function show_custom($user){
        $id = substr($user, 3) - 61197960265728;
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);    
        $response = $client->request('GET', 'players/'.$id);
        $converted = json_decode($response->getBody());

        DB::table('users')->insert(
            [
            'username' => $converted->profile->personaname, 
            'steamid' => $converted->profile->steamid,
            'avatar' => $converted->profile->avatar,
            ]
        );
        $newuser = User::where('steamid', $converted->profile->steamid)->first(); 
        return view('profile.show')->with(compact('newuser'));
    }

    public function refreshStats(User $user)
    {
        $id = $user->convert_steamid_64bit_to_32bit($user->steamid);
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);
        $response = $client->request('POST', 'players/'.$id.'/refresh');
        return redirect()
            ->route('profile.show', ['user' => $user])
            ->with('message','Operation Successful');
    }
}
