<?php

namespace App\Http\Controllers;

use App\Models\Hero;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;

class HeroController extends Controller
{
    public function show(){
        $heroes = Hero::orderBy('id', 'desc')->take(1000)->get();;
        return view('hero.show')->with(compact('heroes'));
    }

    public function show_more(Hero $hero){
        return view('hero.show_more')->with(compact('hero'));
    }

    //This is to create heroes in db!! never call it if there is no update in dota
    public static function createHeroes()
    {
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);    
        $response = $client->request('GET', 'heroes');
        $converted = json_decode($response->getBody());
        
        for ($i=0; $i < count($converted); $i++) { 
            $name = $converted[$i]->name;
            $l_name = $converted[$i]->localized_name;
            $p_attr = $converted[$i]->primary_attr;
            $att_type = $converted[$i]->attack_type;
            DB::insert('insert into heroes 
                (name, localized_name, primary_attr, attack_type) values(?, ?, ?, ?)'
                ,[$name, $l_name, $p_attr, $att_type]);   
        }
    }

}
