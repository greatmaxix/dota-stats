<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Hero extends Model
{
    public $template_base_name = "hero";

    protected $fillable = [
        'name', 'localized_name', 'primary_attr','attack_type', 'hero_img'
    ];

    function getHeroStats(){
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);    
        $response = $client->request('GET', 'heroStats');
        $converted = json_decode($response->getBody(), true); //has stats for all heroes, but we need only for one!!
        //return it as array,not object since it has props which starts with number!!
        //dd($converted);
        $heroWeNeed = $converted[$this->id-1]; //sorted the same way as in db, so we can get it by index!!
        //dd($heroWeNeed);
        return ($heroWeNeed);
    }

    function getHeroMathcUps(){
        $id = $this->id;
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);
        $response = $client->request('GET', 'heroes/'.$id.'/matchups');
        $converted = json_decode($response->getBody());
        $new_array = array();
        foreach ($converted as $hero) {
            $winrate = round(($hero->wins / $hero->games_played) * 100, 2);
            $n_hero = (object) ['hero' => $hero, 'winrate' => $winrate];
            
            array_push($new_array, $n_hero);
        }
        usort($new_array, function($a, $b) { return $b->winrate - $a->winrate; });
        return $new_array;
    }

    public function getById($id){
        $hero = Hero::where('id', $id)->first();
        return $hero;
    }

    static function StaticGetById($id){
        $hero = Hero::where('id', $id)->first();
        return $hero;
    }
}
