<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Match extends Model
{
    public $template_base_name = "match";

    protected $fillable = [
    ];

    static function shower($match){
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);    
        $response = $client->request('GET', 'matches/'.$match);
        $converted = json_decode($response->getBody());
        return $converted;
    }

    static function userWon($match){
        if ($match->player_slot < 128 && $match->radiant_win)
            return true;
        else if ($match->player_slot > 127 && !$match->radiant_win)
            return true;
        return false;
    }
}
