<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','avatar','username','steamid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function getProfileInfo(){
        $id = $this->convert_steamid_64bit_to_32bit($this->steamid);
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);    
        $response = $client->request('GET', 'players/'.$id);
        $converted = json_decode($response->getBody());
        return ($converted);
    }

    function getRecentMatches(){
        $id = $this->convert_steamid_64bit_to_32bit($this->steamid);
        $client = new Client(['base_uri' => 'https://api.opendota.com/api/']);
        $response = $client->request('GET', 'players/'.$id.'/recentMatches');
        $converted = json_decode($response->getBody());
        return $converted;
    }

    function convert_steamid_64bit_to_32bit($id)
    {
        $result = substr($id, 3) - 61197960265728;
        return (string)$result;
    }
}
