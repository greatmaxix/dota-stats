<?php
//dd($match);
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container-fluid">
        <div class="mb-3 card">
            <div class="card bg-light">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4>Match: {{$match->match_id}}</h4>
                        </div>
                        <div class="col-sm-2">
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center"><h4 class="text-info">{{($match->radiant_win ? __('Radiant won') : __('Dire won'))}}</h4></div>
                    </div>
                    <div class="row text-center">
                        <div class="col"><h5 class="text-success">{{$match->radiant_score}}</h5></div>
                        <div class="col"><h5 class="text">{{date('H:i:s',$match->duration)}}</h5></div>
                        <div class="col"><h5 class="text-danger">{{$match->dire_score}}</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-3 card">
            <div class="card bg-light">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4>Dire team</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Hero</th>
                                    <th scope="col">Player</th>
                                    <th scope="col">Kills</th>
                                    <th scope="col">Deaths</th>
                                    <th scope="col">Assists</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($match->players as $player)
                                    @if($player->player_slot > 127)
                                        <tr>
                                            <td> <a href="{{(isset($player->hero_id)) ? route('hero.show_more', ['hero' => App\Models\Hero::StaticGetById($player->hero_id)->id]) : ''}}">
                                                {{App\Models\Hero::StaticGetById($player->hero_id)->localized_name}}</a>
                                            </td>
                                            <td><a href="{{(isset($player->account_id)) ? route('profile.show', ['user' => App\User::where('steamid', $player->account_id)->first()]) : ''}}"> {{isset($player->personaname) ? __($player->personaname) : ''}}</a></td>
                                            <td>{{$player->kills}}</td>
                                            <td>{{$player->deaths}}</td>
                                            <td>{{$player->assists}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-3 card">
            <div class="card bg-light">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4>Radiant team</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Hero</th>
                                    <th scope="col">Player</th>
                                    <th scope="col">Kills</th>
                                    <th scope="col">Deaths</th>
                                    <th scope="col">Assists</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($match->players as $player)
                                    @if($player->player_slot < 128)
                                        <tr>
                                            <td> <a href="{{(isset($player->hero_id)) ? route('hero.show_more', ['hero' => App\Models\Hero::StaticGetById($player->hero_id)->id]) : ''}}">
                                                {{App\Models\Hero::StaticGetById($player->hero_id)->localized_name}}</a>
                                            </td>
                                            <td><a href="{{(isset($player->account_id)) ? route('profile.show', ['user' => App\User::where('steamid', $player->account_id)->first()]) : ''}}"> {{isset($player->personaname) ? __($player->personaname) : ''}}</a></td>
                                            <td>{{$player->kills}}</td>
                                            <td>{{$player->deaths}}</td>
                                            <td>{{$player->assists}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection