<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <div class="container">
        <div class="collapse navbar-collapse">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{'Dota stats'}}
            </a>
            <ul class="nav navbar-nav navbar-right ml-auto">
                <li class="nav-item btn btn-outline-info mx-2">
                    <a class="nav-link" href="{{ route('hero.show')}}"> {{ __('Heroes')}}</a>
                </li>
                @guest
                <li class="nav-item btn btn-outline-success mx-2">
                    <a class="nav-link" href="{{ route('auth.steam') }}">{{ __('Login using Steam') }}</a>
                </li>
                @else
                <li class="nav-item mx-2">
                    <a class="navbar-brand" href="#">
                        <img class="rounded-circle" src="{{ Auth::user()->avatar }}" alt="avatar" style="width:40px;">
                    </a>
                </li>
                <li class="nav-item btn btn-outline-success mx-2">
                    <a class="nav-link" href="{{ route('profile.show', ['user' => AUTH::user()])}}">
                        {{ Auth::user()->username }} <span class="caret"></span>
                    </a>
                </li>
                <li class="nav-item btn btn-outline-danger mx-2">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>