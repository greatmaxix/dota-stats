@extends('layouts.app')

@section('content')
<div class="container">
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 font-weight-normal">Dota stats</h1>
            <p class="lead font-weight-normal">
                Your personal statistics,
                Match statistics, Hero stats and more.</p>
            @if (!Auth::check())
                <a class="btn btn-outline-secondary" href="{{ route('auth.steam') }}">{{ __('Login using Steam') }}</a>
            @endif
                <a class="btn btn-outline-secondary" href="{{ route('hero.show') }}">{{ __('Hero stats') }}</a>
        </div>
    </div>
</div>
@endsection
