<?php
$profile_data = $user->getProfileInfo();
$recent_mathces = $user->getRecentMatches();
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container-fluid">
        <div class="mb-3 card">
            <div class="card bg-light">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-2">
                            Info
                        </div>
                        <form class="col float-right" action="{{ route('profile.show', ['user' => $user]) }}" method="POST">
                            @csrf
                            <div class="col float-right">
                                <button class="btn btn-primary" type="submit">{{ __('Refresh') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <p class="col-sm-1"> <img class="rounded" src="{{$profile_data->profile->avatarmedium}}" alt="avatar" style="width:40px;"/></p>
                        <h5 class="col">
                            {{$profile_data->profile->personaname}} 
                        </h5>
                    </div>
                    <p class="row text-secondary pl-2">Estimated MMR: {{$profile_data->mmr_estimate->estimate}}</p>
                    <p class="row text-secondary pl-2">Country: {{$profile_data->profile->loccountrycode}}</p>
                    <p class="row text-secondary pl-2">Last login: {{$profile_data->profile->last_login}}</p>
                </div>
            </div>
        </div>
        <div class="mb-3 card">
            <div class="card bg-light">
                <div class="card-header">
                    Recent games
                </div>
                <div class="card-body">
                    <div class="row">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Match id</th>
                                    <th scope="col">Hero</th>
                                    <th scope="col">Kills</th>
                                    <th scope="col">Deaths</th>
                                    <th scope="col">Assists</th>
                                    <th scope="col">Duration</th>
                                    <th scope="col">Win</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($recent_mathces as $match)
                                    <tr>
                                    <th scope="row"> <a href="{{route('match.show', ['match' => $match->match_id])}}">{{$match->match_id}}</a></th>
                                    <td> <a href="{{route('hero.show_more', ['hero' => App\Models\Hero::StaticGetById($match->hero_id)->id])}}">
                                        {{App\Models\Hero::StaticGetById($match->hero_id)->localized_name}}</a>
                                    </td>
                                    <td>{{$match->kills}}</td>
                                    <td>{{$match->deaths}}</td>
                                    <td>{{$match->assists}}</td>
                                    <td>{{date('H:i:s',$match->duration)}}</td>
                                    <td>{{App\Models\Match::userWon($match) ? __('Win') : __('Lost')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection