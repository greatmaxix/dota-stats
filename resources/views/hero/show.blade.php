@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container-fluid">
        <div class="card-deck">
            <div class="row">
            @foreach ($heroes as $hero)
                <div class="col-sm-3">
                    <a href="{{route('hero.show_more', ['hero' => $hero])}}">
                        <div class="cardy 1 mx-2">
                            <div class="cardy_image"> <img alt="{{$hero->name}}" src="data:image/png;base64,{{chunk_split(base64_encode($hero->hero_img)) }}" /> </div>
                            <div class="cardy_title title-white">
                                <p>{{$hero->localized_name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            </div>
    </div>
</div>
      
@endsection

