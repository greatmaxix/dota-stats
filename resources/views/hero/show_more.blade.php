<?php
$stats = $hero->getHeroStats();
$matchUps = $hero->getHeroMathcUps();
$top5against = array_slice($matchUps, 0, 5, true);
$top5bad = array_slice($matchUps, -5);
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container-fluid">
        <div class="mb-3 card">
            <div class="card-header-tab card-header ">
                <div class="row">
                     <div class="col-sm-2">
                        <img class="rounded" id="myImg" style="width:100px;" alt="hero_icon" src="data:image/png;base64,{{chunk_split(base64_encode($hero->hero_img))}}"/>
                        <div id="myModal" class="modal">
                            <!-- The Close Button -->
                            <span class="close">&times;</span>
                            <!-- Modal Content (The Image) -->
                            <img class="modal-content" id="img01"> 
                            <!-- Modal Caption (Image Text) -->
                            <div id="caption"></div>
                        </div>
                        <link href="{{ URL::asset('css/imgzoomable.css') }}" rel="stylesheet" type="text/css" >
                        <script src="{{ URL::asset('js/imgzoomable.js') }}"></script>
                    </div>
                    <div class="col-sm-6">
                        <h3 class="header-icon lnr-charts icon-gradient bg-happy-green">{{$hero->localized_name}}</h3>
                    </div>
                    <div class="icon-wrapper rounded-circle col-sm-2">
                        @if($hero->primary_attr == 'int')
                            <i><img src="{{asset('img/int_icon.png')}}"/></i>
                        @elseif($hero->primary_attr == 'str')
                            <i><img src="{{asset('img/str_icon.png')}}"/></i>
                        @else
                            <i><img src="{{asset('img/agi_icon.png')}}"/></i>
                        @endif
                    </div>
                    <div class="col-sm-2">
                        @foreach ($stats['roles'] as $role)
                            <div class="row mb-2">
                                <span class="badge badge-primary">{{$role}}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="no-gutters row">
                @for ($i = 1; $i < 9; $i++)
                    <div class="col-sm-3">
                        <div class="card align-items-center">
                            <i class="">
                                <img class="img-fluid" style="width:50px" src="{{asset('img/ranks/'.$i.'.png')}}"/>    
                            </i>
                            <p>Games Played: {{$stats[''.$i.'_pick']}}</p>
                            <span class="pl-1">{{round(($stats[''.$i.'_win'] / $stats[''.$i.'_pick']) * 100, 2)}}% Winrate</span>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
        <div class="mb-3 card">
            <div class="card-header-tab card-header ">
                <div class="card-body">
                    <h4 class="card-title text-success">Good against:</h4>
                    <div class="row">
                        @foreach ($top5against as $h)
                            <div class="col-sm-2">
                                <a href="{{route('hero.show_more', ['hero' => $h->hero->hero_id])}}">
                                    <div class="cardy cardy_small 1 mx-2">
                                        <div class="cardy_image"> 
                                            <img alt="{{$hero->name}}" src="data:image/png;base64,{{chunk_split(base64_encode(($hero->getById($h->hero->hero_id))->hero_img)) }}" /> 
                                        </div>
                                        <div class="cardy_title title-white">
                                            <p>{{$h->winrate}}% wins</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-3 card">
                <div class="card-header-tab card-header ">
                    <div class="card-body">
                        <h4 class="card-title text-danger">Bad against:</h4>
                        <div class="row">
                            @foreach ($top5bad as $h)
                                <div class="col-sm-2">
                                    <a href="{{route('hero.show_more', ['hero' => $h->hero->hero_id])}}">
                                        <div class="cardy 1 mx-2">
                                            <div class="cardy_image"> 
                                                <img alt="{{$hero->name}}" src="data:image/png;base64,{{chunk_split(base64_encode(($hero->getById($h->hero->hero_id))->hero_img)) }}" /> 
                                            </div>
                                            <div class="cardy_title title-white">
                                                <p>{{$h->winrate}}% wins</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection