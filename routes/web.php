<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('auth/steam', 'AuthController@redirectToSteam')->name('auth.steam');
Route::get('auth/steam/handle', 'AuthController@handle')->name('auth.steam.handle');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/heroes', 'HeroController@show')->name('hero.show');
Route::get('/heroes/{hero}', 'HeroController@show_more')->name('hero.show_more');

Route::get('matches/{match}', 'MatchController@shower')->name('match.show');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/profile/{user}', 'ProfileController@show')->name('profile.show');
    Route::post('/profile/{user}', 'ProfileController@refreshStats');
});
//db check
Route::get('/db-test', function() {
    if(DB::connection()->getDatabaseName()){
       echo "connected sucessfully to database" . DB::connection()->getDatabaseName();
    }
 });